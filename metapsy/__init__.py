from colorama import Fore, Style

VERSION_PYTHON = '0.1.0'
VERSION_R_DATA = '0.3.1'
VERSION_R_TOOLS = '0.3.3-5'

print(
    Fore.GREEN + '\u2713' +
    Fore.CYAN +
    ' Loading metapsy '
    f"v{VERSION_PYTHON} (based on the R packages "
    f"{{metapsyData}} v{VERSION_R_DATA} and "
    f"{{metapsyTools}} v{VERSION_R_TOOLS}).\n"
    '\u2192 For help, go to ' +
    Fore.GREEN +
    'data.metapsy.org' +
    Fore.CYAN +
    '.\n'
    '\u2192 To access data with the `data` module (analogous to the '
    '{metapsyData} R package), make sure your computer is connected to the '
    'Internet.\n'
    '\u2192 Use the `tools` module (analogous to the {metapsyTools} R package) '
    'to run meta-analyses. ' +
    Fore.YELLOW +
    '[This module has yet to be implemented]' +
    Style.RESET_ALL
)
