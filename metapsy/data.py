import datetime
import json
import math
from io import StringIO

import pandas as pd
import requests
from colorama import Fore, Style


class MetapsyDatabase:

    data: pd.DataFrame
    title: str
    database_doi: str
    documentation_url: str
    github_repo_url: str
    last_search: datetime.date
    last_updated: datetime.date
    license: str
    repository_download_url: str
    variable_description: str
    version: str
    version_doi: str

    def __init__(
            self,
            data: pd.DataFrame,
            title: str,
            version: str,
            last_updated: datetime.date,
            last_search: datetime.date,
            database_doi: str,
            version_doi: str,
            repository_download_url: str,
            github_repo_url: str,
            documentation_url: str,
            license: str,
            variable_description: str,
    ):
        self.data = data
        self.title = title
        self.version = version
        self.last_updated = last_updated
        self.last_search = last_search
        self.database_doi = database_doi
        self.version_doi = version_doi
        self.repository_download_url = repository_download_url
        self.github_repo_url = github_repo_url
        self.documentation_url = documentation_url
        self.license = license
        self.variable_description = variable_description

    # TODO: Implement all these not essential methods
    def download_zip(self):
        ...

    def open_documentation(self):
        ...

    def open_git_repo(self):
        ...

    def variable_description(self):
        ...

    def return_metadata(self):
        ...

    def __repr__(self):
        return str(self.data)


def transform_float(inputstr: str | float) -> float:
    """
    Parse string representation of floating-point number like '14.5', '4.0'
    and 'nan' into actual floating-point number.

    :param inputstr: String representation of floating-point number
    :return: Floating-point number or nan
    """
    # TODO: Increase precision from 5 post-points-digits to at least 16
    #  post-point-digits
    # Explicitly convert `inputstr` to str because its `nan` value is of type
    # float
    return float(str(inputstr).replace(',', '.'))


def transform_int(inputfloat: float | str) -> int | None:
    """
    Parse float representation of integer like '14.0', '8.0' and 'nan'
    into actual integer.

    :param inputfloat: Float representation of integer
    :return: Integer or nan
    """
    if isinstance(inputfloat, str):
        try:
            out = int(inputfloat)
        except ValueError:
            out = float(inputfloat.replace(',', '.'))
        return
    else:
        if math.isnan(inputfloat):
            return None


def get_data(
        shorthand: str,
        version=None,
        include_metadata=True,
) -> pd.DataFrame | MetapsyDatabase:
    # VERSION CHECK ============================================================
    if version is not None:
        print(
            '- ' +
            Fore.GREEN +
            '[OK]' +
            Style.RESET_ALL +
            f" Downloading {shorthand} database (version {version}) ..."
        )
    else:
        print(
            '- ' +
            Fore.GREEN +
            '[OK]' +
            Style.RESET_ALL +
            f" Downloading {shorthand} database ..."
        )

    # GET AVAILABLE SHORTHANDS =================================================
    data_index_url = (
        'https://raw.githubusercontent.com/metapsy-project/metapsyData/master/'
        'pkgdown/assets/data/data-index.csv'
    )
    data_index_request_response = requests.get(data_index_url)
    df_data_index = pd.read_csv(
        StringIO(data_index_request_response.text),
        sep=';'
    )

    if shorthand not in df_data_index['shorthand'].iloc:
        raise Exception(
            f"Shorthand {shorthand} not found. "
            "All available datasets and their respective shorthands are "
            "documented at docs.metapsy.org/databases (see 'Metadata' section)."
        )
    this_doi = df_data_index[
        df_data_index['shorthand'] == shorthand
    ].iloc[0]['doi']

    # RETRIEVE METADATA FOR SPECIFIED SHORTHAND ================================
    metadata_url = (
        'https://zenodo.org/api/deposit/depositions?'
        'access_token=Bounk4ySHPIYxrFMWN49jyenJZ1Uy6t'
        'Bhico7tuZ3iW6cp1hJ3m9FIY6HcvX&all_versions=1&size=10000'
    )
    metadata_response = requests.get(metadata_url)
    metadata_dict = json.loads(metadata_response.text)
    df_metadata = pd.DataFrame.from_dict(
        metadata_dict,
        orient='columns'
    )
    this_db_metadata = df_metadata[df_metadata['conceptdoi'] == this_doi]

    # Print retrieved version ----------------------------------------
    all_versions = [
        d.get('version')
        for d
        in this_db_metadata['metadata'].iloc
    ]
    if version is None:
        version = all_versions[0]
        print(
            '- ' +
            Fore.GREEN +
            '[OK]' +
            Style.RESET_ALL +
            f" Retrieving latest version ({version}) ..."
        )
    else:
        if version not in all_versions:
            raise Exception('The specified database version was not found.')
        print(
            '- ' +
            Fore.GREEN +
            '[OK]' +
            Style.RESET_ALL +
            f" Retrieving version ({version}) ..."
        )

    metapsy_database_object = None
    if include_metadata:
        this_version_metadata = this_db_metadata.iloc[
            all_versions.index(version)
        ]
        repo = df_data_index[
            df_data_index['shorthand'] == shorthand
        ].iloc[0]['repo']
        last_search_url = (
            'https://raw.githubusercontent.com/metapsy-project/'
            f"{repo}/{version}/metadata/last_search.txt"
        )
        last_search = datetime.datetime.strptime(
            requests.get(last_search_url).text.strip(),
            '%Y-%m-%d'
        ).date()
        documentation_url = df_data_index[
            df_data_index['shorthand'] == shorthand
        ].iloc[0]['url'],
        variable_description_url = (
            'https://raw.githubusercontent.com/metapsy-project/'
            f"{repo}/{version}/metadata/variable_description.json"
        )
        variable_description = json.loads(
            requests.get(variable_description_url).text
        )

        metapsy_database_object = MetapsyDatabase(
            data=None,
            title=this_version_metadata['title'],
            version=version,
            last_updated=datetime.datetime.strptime(
                this_version_metadata['modified'],
                '%Y-%m-%dT%H:%M:%S.%f',
            ),
            last_search=last_search,
            database_doi=this_version_metadata['conceptdoi'],
            version_doi=this_version_metadata['doi'],
            repository_download_url=(
                this_version_metadata['files'][0]
                .get('links')
                .get('download')
            ),
            github_repo_url=(
                this_version_metadata['metadata']
                .get('related_identifiers')[0]
                .get('identifier')
            ),
            documentation_url=(
                f"https://docs.metapsy.org/databases/{documentation_url}/"
            ),
            license=this_version_metadata['metadata'].get('license'),
            variable_description=variable_description,
        )

    # DOWNLOAD DATA ============================================================
    this_repo = df_data_index[
        df_data_index['shorthand'] == shorthand
    ].iloc[0]['repo']
    data_url = (
        f"https://raw.githubusercontent.com/metapsy-project/{this_repo}/"
        f"{version}/data.csv"
    )
    data_response = requests.get(data_url)
    df_data = pd.read_csv(
        StringIO(data_response.text),
        sep=';'
    )

    # Parse numerics -------------------------------------------------
    df_data_clean = df_data.copy()
    for col in (
        'time_weeks',
        'mean_arm1',
        'mean_arm2',
        'sd_arm1',
        'sd_arm2',
        'mean_change_arm1',
        'mean_change_arm2',
        'sd_change_arm1',
        'sd_change_arm2',
        'precalc_g',
        'precalc_g_se',
        'precalc_log_rr',
        'precalc_log_rr_se',
        'baseline_m_arm1',
        'baseline_sd_arm1',
        'baseline_m_arm1',
        'baseline_sd_arm2',
        'n_sessions_arm1',
        'mean_age',
        'percent_women',
        '.g',
        '.g_se',
        '.log_rr',
        '.log_rr_se',
    ):
        if col in df_data_clean.columns:
            df_data_clean[col] = df_data_clean[col].map(transform_float)
    for col in (
        'n_arm1',
        'n_arm2',
        'n_change_arm1',
        'n_change_arm2',
        'event_arm1',
        'event_arm2',
        'totaln_arm1',
        'totaln_arm2',
        'baseline_n_arm1',
        'baseline_n_arm2',
        'year',
        'ac',
        'itt',
        'sg',
        'rob',
        'no.arms',
        'is.multiarm',
        'primary_calc',
        '.totaln_arm1',
        '.totaln_arm2',
    ):
        if col in df_data_clean.columns:
            df_data_clean[col] = df_data_clean[col].map(transform_int)

    # Study and id are never parsed ----------------------------------
    # TODO: Implement

    # RETURN ===================================================================
    if include_metadata:
        metapsy_database_object.data = df_data_clean
        out = metapsy_database_object
    else:
        out = df_data_clean

    print(
        '- ' +
        Fore.GREEN +
        '[OK]' +
        Style.RESET_ALL +
        ' Download successful!'
    )

    return out


def list_data() -> pd.DataFrame:
    data_index_url = (
        'https://raw.githubusercontent.com/metapsy-project/metapsyData/master/'
        'pkgdown/assets/data/data-index.csv'
    )
    data_index_response = requests.get(data_index_url)
    df_data_index = pd.read_csv(
        StringIO(data_index_response.text),
        sep=';'
    ).set_index('shorthand')
    df_data_index['url'] = 'docs.metapsy.org/databases/' + df_data_index['url']

    print(
        '- ' +
        Fore.GREEN +
        '[OK]' +
        Style.RESET_ALL +
        'Retrieving available databases...'
    )

    return df_data_index


# df_out = get_data('depression-anxiety-transdiagnostic')
# df_out = get_data('depression-inpatients')
# df_out = get_data('depression-psyctr')
# df_list = list_data()
